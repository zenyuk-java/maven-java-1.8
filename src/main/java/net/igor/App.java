package net.igor;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;


public class App
{
    public static void main( String[] args )
    {
        System.out.println( "wow" );
        Map<Integer, String> m = new TreeMap<>();
        m.put(1, "odin");
        m.put(0, "null");
        m.put(2, "dva");
        m.put(2, "dvadva");
        m.put(3, "zeroq");

        Comparator cmp = new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                int result = 0;

                if (i1 < i2)
                    result = 1;
                if (i1 > i2)
                    result = -1;

                return result;

            }
        };
        Map<Integer, String> r = new TreeMap<>(cmp);

        m.keySet().stream()
            .filter(k -> !m.get(k).startsWith("d")
                && m.get(k).length() > 3)
            .forEach(k -> r.put(k, m.get(k)));

        System.out.println(r);
    }
}
